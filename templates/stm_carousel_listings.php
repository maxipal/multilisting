<?php
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);
$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class($css, ' '));

$asCarousel = (!empty($as_carousel)) ? $as_carousel : 'no';

$randId = 'owl' . rand(1, 10000000);
$vis_limit = empty($vis_limit) ? 2 : $vis_limit;

if(!empty($filter_selected)){
	$filter_selected = explode(',',$filter_selected);
	$filter_selected = $filter_selected[0];
}

$listings = STMMultiListing::getListings();
if(empty($limit)) $limit = 8;

$args = [
	'post_type' => $post_type,
	'posts_per_page' => $limit,
	'post_status' => 'publish',

];

$autoplay = $autoplay ? 'true' : 'false';

if (!empty($filter_selected)) {
//	$filter_selected_info = stm_get_all_by_slug($filter_selected);
//	$multiply = false;
//	if (!empty($filter_selected_info['listing_rows_numbers'])) {
//		$multiply = true;
//	}
//
//	$hideEmpty = ($asCarousel == 'no') ? false : true;
//
//	$args = array(
//		'orderby' => 'name',
//		'order' => 'ASC',
//		'hide_empty' => $hideEmpty,
//		'pad_counts' => true
//	);
//
//	$terms = get_terms($filter_selected, $args);
//
//	$terms_images = array();
//	$terms_text = array();
//	if (!empty($terms)) {
//		foreach ($terms as $term) {
//			$image = get_term_meta($term->term_id, 'stm_image', true);
//			if (empty($image)) {
//				$terms_text[] = $term;
//			} else {
//				$terms_images[] = $term;
//			}
//		};
//	}
//
//	if ( empty($limit) && $asCarousel == 'no' ) {
//		$limit = 20;
//	}
//
//	if($asCarousel == 'yes') {
//		$limit = 100;
//	}
}

$listings = new WP_Query($args);
?>

<div class="wrap_multilisting_carousel">
	<div class="wrap_multilisting_carousel_inner car-listing-row" id="<?php echo esc_attr($randId); ?>">
		<?php while($listings->have_posts()): $listings->the_post(); ?>
			<?php
			$regular_price_label = get_post_meta(get_the_ID(), 'regular_price_label', true);
			$special_price_label = get_post_meta(get_the_ID(),'special_price_label',true);

			$price = get_post_meta(get_the_id(),'price',true);
			$sale_price = get_post_meta(get_the_id(),'sale_price',true);

			$car_price_form_label = get_post_meta(get_the_ID(), 'car_price_form_label', true);

			$data = array(
				'data_price' => 0,
				'data_mileage' => 0,
			);

			if(!empty($price)) {
				$data['data_price'] = $price;
			}

			if(!empty($sale_price)) {
				$data['data_price'] = $sale_price;
			}

			if(empty($price) and !empty($sale_price)) {
				$price = $sale_price;
			}

			$mileage = get_post_meta(get_the_id(),'mileage',true);

			if(!empty($mileage)) {
				$data['data_mileage'] = $mileage;
			}

			$data['class'] = array('stm-directory-grid-loop stm-isotope-listing-item all');
			$asSold = get_post_meta(get_the_ID(), 'car_mark_as_sold', true);
			?>

			<div class="stm-directory-grid-loop stm-isotope-listing-item all stm-directory-grid-loop stm-isotope-listing-item all <?php if (!empty($asSold)) echo esc_attr('car-as-sold'); ?>">
				<a href="<?php the_permalink(); ?>" class="rmv_txt_drctn">
					<?php stm_listings_load_template('loop/classified/grid/image', $data); ?>

					<div class="listing-car-item-meta">
						<?php stm_listings_load_template(
							'loop/default/grid/title_price',
							array(
								'price' => $price,
								'sale_price' => $sale_price,
								'car_price_form_label' => $car_price_form_label
							)); ?>

						<?php stm_listings_load_template('loop/classified/grid/data'); ?>

					</div>
				</a>
			</div>
		<?php endwhile; ?>
	</div>
</div>

<style>
	.wrap_multilisting_carousel{
		padding-right: 15px;
		padding-left: 15px;
	}
	.wrap_multilisting_carousel_inner{
		visibility: hidden;
		display: none;
	}
	.wrap_multilisting_carousel_inner.owl-carousel .owl-controls {
		display: block;
		width: 100%;
		position: absolute;
		top: 100px;
		transform: translateY(-38%);
		margin: 0;
	}
	.wrap_multilisting_carousel_inner.owl-carousel .owl-controls .owl-nav {
		display: block;
		z-index: 100;
		width: 100%;
		position: relative;
	}
	.wrap_multilisting_carousel_inner.owl-carousel .owl-controls .owl-nav .owl-prev,
	.wrap_multilisting_carousel_inner.owl-carousel .owl-controls .owl-nav .owl-next {
		width: 25px;
		height: 55px;
		top: 50%;
		display: block;
		font-size: 0;
		position: absolute;
		background: #ddd;
		border-radius: 5px;
		transform: translateY(-50%);
		color: transparent;
	}
	.wrap_multilisting_carousel_inner.owl-carousel .owl-controls .owl-nav .owl-prev {
		left: -20px;
	}
	.wrap_multilisting_carousel_inner.owl-carousel .owl-controls .owl-nav .owl-next {
		right: -20px;
	}
	.wrap_multilisting_carousel_inner.owl-carousel .owl-controls .owl-nav .owl-prev:before,
	.wrap_multilisting_carousel_inner.owl-carousel .owl-controls .owl-nav .owl-next:before {
		display: block;
		height: auto;
		font-family: 'FontAwesome';
		color: #fff;
		font-size: 16px;
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translateY(-50%) translateX(-50%);
		font-weight: 400;
		-moz-osx-font-smoothing: grayscale;
		-webkit-font-smoothing: antialiased;
		text-rendering: auto;
	}
	.wrap_multilisting_carousel_inner.owl-carousel .owl-controls .owl-nav .owl-prev:before {
		content: '\f053';
	}
	.wrap_multilisting_carousel_inner.owl-carousel .owl-controls .owl-nav .owl-next:before {
		content: '\f054';
	}
	.wrap_multilisting_carousel_inner.owl-carousel .owl-item img{
		height: 132px;
		object-fit: cover;
		width: 100%;
		object-position: center;
	}
	.wrap_multilisting_carousel_inner.owl-carousel .stm-directory-grid-loop{
		padding-left: 10px;
		padding-right: 10px;
	}
</style>
<script>
(function($) {
	$(document).ready(function () {
		var owlIcon = $('#<?php echo esc_attr($randId); ?>');
		var owlRtl = false;
		if( $('body').hasClass('rtl') ) {
			owlRtl = true;
		}

		owlIcon.owlCarousel({
			items: <?php echo (int)$vis_limit ?>,
			smartSpeed: 800,
			dots: false,
			margin: 0,
			autoplay: <?php echo $autoplay ?>,
			nav: true,
			loop: true,
			responsiveRefreshRate: 1000,
			responsive:{
				0:{
					items:1
				},
				500:{
					items:1
				},
				768:{
					items:2
				},
				1000:{
					items:<?php echo (int)$vis_limit ?>
				}
			}
		})
	});
})(jQuery);
</script>
