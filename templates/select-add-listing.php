<?php get_header(); ?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="wrap_listings_list">

				<h1 class="h3">
					<?php _e("Select type of listing", MULTILISTING_DOMAIN) ?>
				</h1>
				<?php echo do_shortcode('[stm_multilisting_add]') ?>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>
