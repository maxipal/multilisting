<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_class = (!empty($css)) ? apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class($css, ' ')) : '';

?>
<?php
$listings = STMMultiListing::getListings();
$settings = STMMultiListing::getListingsSettings();
?>
<div class="wrap_add_multilisting">
	<?php if(!empty($listings)): ?>
		<ul class="wrap_listings_list_inner clearfix">
			<?php if(!isset($settings['disable_orig_listing']) || $settings['disable_orig_listing'] == 'false'): ?>
				<?php $listing_btn_link = get_theme_mod('header_listing_btn_link', '/add-a-car'); ?>
				<li class="ml_item_listings">
					<a href="<?php echo esc_url($header_listing_btn_link); ?>">
						<i class="fa fa-list-alt"></i>
						<span><?php _e("Listings") ?></span>
					</a>
				</li>
			<?php endif ?>
			<?php foreach ($listings as $listing): ?>
				<li class="ml_item_<?php echo $listing['slug'] ?>">
					<a href="<?php echo(!empty($listing['page']) ? get_permalink($listing['page']) : '#') ?>">
						<i class="
						<?php if(empty($listing['icon']))
							echo 'fa fa-list-alt';else echo $listing['icon'] ?>"></i>
						<span><?php echo $listing['name'] ?></span>
					</a>
				</li>
			<?php endforeach; ?>
		</ul>
	<?php endif ?>
</div>
