<?php
/**
 * Plugin Name: STM Multi-Listing
 * Description: Multi-Listing for Motors theme
 * Author: Stylemix WordPress Team
 * Author URI: https://stylemix.net/
 * Text Domain: multi_listing
 * Version: 1.1.11
 */

if (!defined('ABSPATH')) exit;

define('MULTILISTING_DOMAIN', 'multi_listing');
define('MULTILISTING_PLUGIN_URL', plugins_url().'/'.dirname( plugin_basename( __FILE__ ) ));
define('MULTILISTING_PATH', plugin_dir_path( __FILE__ ));


require_once MULTILISTING_PATH . "/admin/listing-dashboard.php";
require_once MULTILISTING_PATH . "/admin/vc_elements.php";

require_once MULTILISTING_PATH . "/classes/multilisting.class.php";
require_once MULTILISTING_PATH . "/classes/categories.class.php";
require_once MULTILISTING_PATH . "/classes/butterbean.class.php";
require_once MULTILISTING_PATH . "/classes/metabox.class.php";
require_once MULTILISTING_PATH . "/classes/templates.class.php";
require_once MULTILISTING_PATH . "/classes/hooks.class.php";

