<table class="form-table">
	<tbody>
		<tr class="disable-original-listing">
			<th>
				<label for="disable_orig_listing">
					<?php _e("Disable Original Car Listing",MULTILISTING_DOMAIN) ?>
				</label>
			</th>
			<td>
				<input
					type="checkbox"
					name="'wpuniq_theme_multilisting_motors[settings][disable_orig_listing]'"
					id="disable_orig_listing"
					v-model="settings.disable_orig_listing">
					<span class="description">
						<?php _e("Disable",MULTILISTING_DOMAIN) ?>
					</span>
			</td>
		</tr>

		<tr class="add-listing-endpoint">
			<th>
				<label for="add_listing_endpoint">
					<?php _e("Add Listing Endpoint",MULTILISTING_DOMAIN) ?>
				</label>
			</th>
			<td>
				<input
					type="text"
					name="'wpuniq_theme_multilisting_motors[settings][add_listing_name]'"
					id="add_listing_endpoint"
					placeholder="<?php _e("Type slug (ex. 'add-listing')", MULTILISTING_DOMAIN ) ?>"
					v-model="settings.add_listing_name">
			</td>
		</tr>

	</tbody>
</table>
