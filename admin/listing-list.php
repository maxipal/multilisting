<div class="wrap_mm_columns">
	<div class="wrap_listing_list">
	<div class="listing_list_inner">
		<ul>
			<li v-for="(listing,index) in listings" :key="index" :ref="'el_'+index">
				<span>
					<input
						type="text"
						:ref="'el_'+index+'_name'"
						placeholder="<?php _e("Label for new listing", MULTILISTING_DOMAIN) ?>"
						v-on:keyup="setSlug(index, event)"
						:name="'wpuniq_theme_multilisting_motors['+index+'][name]'"
						v-model="listing.name">
				</span>
				<span>
					<input
						type="text"
						placeholder="<?php _e("Slug for new listing", MULTILISTING_DOMAIN) ?>"
						:ref="'el_'+index+'_slug'"
						:name="'wpuniq_theme_multilisting_motors['+index+'][slug]'"
						v-model="listing.slug">
				</span>
				<span>
					<select
						:ref="'el_'+index+'_page'"
						:name="'wpuniq_theme_multilisting_motors['+index+'][page]'"
						v-model="listing.page">
						<option v-for="(page, key) in pages" :key="key" :value="key">
							{{page}}
						</option>
					</select>
				</span>
				<span>
					<input
						placeholder="<?php _e("Icon class(FA)", MULTILISTING_DOMAIN) ?>"
						type="text"
						:ref="'el_'+index+'_icon'"
						:name="'wpuniq_theme_multilisting_motors['+index+'][icon]'"
						v-model="listing.icon">
				</span>
				<span class="wrap_listing_action">
					<a
						href="#"
						v-on:click.stop.prevent="showSettingModal(index, event)"
						title="<?php _e("Settings", MULTILISTING_DOMAIN) ?>">
						<svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="sliders-v" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-sliders-v fa-w-14 fa-3x"><path fill="currentColor" d="M160 168v-48c0-13.3-10.7-24-24-24H96V8c0-4.4-3.6-8-8-8H72c-4.4 0-8 3.6-8 8v88H24c-13.3 0-24 10.7-24 24v48c0 13.3 10.7 24 24 24h40v312c0 4.4 3.6 8 8 8h16c4.4 0 8-3.6 8-8V192h40c13.3 0 24-10.7 24-24zm-32-8H32v-32h96v32zm152 160h-40V8c0-4.4-3.6-8-8-8h-16c-4.4 0-8 3.6-8 8v312h-40c-13.3 0-24 10.7-24 24v48c0 13.3 10.7 24 24 24h40v88c0 4.4 3.6 8 8 8h16c4.4 0 8-3.6 8-8v-88h40c13.3 0 24-10.7 24-24v-48c0-13.3-10.7-24-24-24zm-8 64h-96v-32h96v32zm152-224h-40V8c0-4.4-3.6-8-8-8h-16c-4.4 0-8 3.6-8 8v152h-40c-13.3 0-24 10.7-24 24v48c0 13.3 10.7 24 24 24h40v248c0 4.4 3.6 8 8 8h16c4.4 0 8-3.6 8-8V256h40c13.3 0 24-10.7 24-24v-48c0-13.3-10.7-24-24-24zm-8 64h-96v-32h96v32z" class=""></path></svg>
					</a>
					<a
						v-if="listings.length == index + 1"
						href="#"
						v-on:click.stop.prevent="duplicate(index, event)"
						title="<?php _e("Add", MULTILISTING_DOMAIN) ?>">
						<svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="plus-square" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-plus-square fa-w-14 fa-3x"><path fill="currentColor" d="M352 240v32c0 6.6-5.4 12-12 12h-88v88c0 6.6-5.4 12-12 12h-32c-6.6 0-12-5.4-12-12v-88h-88c-6.6 0-12-5.4-12-12v-32c0-6.6 5.4-12 12-12h88v-88c0-6.6 5.4-12 12-12h32c6.6 0 12 5.4 12 12v88h88c6.6 0 12 5.4 12 12zm96-160v352c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V80c0-26.5 21.5-48 48-48h352c26.5 0 48 21.5 48 48zm-48 346V86c0-3.3-2.7-6-6-6H54c-3.3 0-6 2.7-6 6v340c0 3.3 2.7 6 6 6h340c3.3 0 6-2.7 6-6z" class=""></path></svg>
					</a>
					<a
						class="btn_red"
						v-if="listings.length !== index + 1"
						href="#"
						@click="remove(index, event)"
						title="<?php _e("Remove", MULTILISTING_DOMAIN) ?>">
						<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="trash-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-trash-alt fa-w-14 fa-3x"><path fill="currentColor" d="M32 464a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z" class=""></path></svg>
					</a>
				</span>
			</li>
		</ul>
		<div ref="modal" class="modal fade" tabindex="-1" role="dialog" v-show="modalShow" style="display: none">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Settings</h5>
						<button @click="modalShow=false" type="button" class="close" data-dismiss="modal" aria-label="<?php _e("Close", MULTILISTING_DOMAIN) ?>">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="separate_paragraph">
							<p>
								<label for="inventory_id" class="label_clear_row"><?php _e("Select Inventory page", MULTILISTING_DOMAIN) ?></label>
								<select id="inventory_id" v-model="modal.inventory_id" class="label_clear_row">
									<option value="">
										<?php _e("Select page", MULTILISTING_DOMAIN) ?>
									</option>
									<?php foreach(get_pages() as $page): ?>
										<option value="<?php echo $page->ID ?>">
											<?php echo $page->post_title ?>
										</option>
									<?php endforeach ?>
								</select>
							</p>
						</div>

						<div class="separate_paragraph">
							<p>
								<input type="checkbox" id="pay_per_listing" v-model="modal.ppl">
								<label for="pay_per_listing"><?php _e("Enable Pay Per Listing", MULTILISTING_DOMAIN) ?></label>
							</p>
							<p v-show="modal.ppl">
								<label for="ppl_price"><?php _e("Pay Per Listing Price", MULTILISTING_DOMAIN) ?></label>
								<input id="ppl_price" type="text" v-model="modal.ppl_price">
							</p>
							<p v-show="modal.ppl">
								<label for="ppl_limit"><?php _e("Pay Per Listing Period (days)", MULTILISTING_DOMAIN) ?></label>
								<input id="ppl_limit" type="text" v-model="modal.ppl_limit">
							</p>
						</div>
						<div class="separate_paragraph">
							<p>
								<label for=""><?php _e("Featured Listing Price", MULTILISTING_DOMAIN) ?></label>
								<input type="text" v-model="modal.featured_price">
							</p>
							<p>
								<label for=""><?php _e("Featured Listing Period (days)", MULTILISTING_DOMAIN) ?></label>
								<input type="text" v-model="modal.featured_limit">
							</p>
						</div>

						<div class="separate_paragraph">
							<p>
								<label for="generate_title"><?php _e("Display generated listing title as", MULTILISTING_DOMAIN) ?>:</label>
								<input id="generate_title" type="text" v-model="modal.generate_title">
							</p>
							<p class="ml_description">
								<?php _e("Put in curly brackets slug of taxonomy. For Example - {make} {serie} {ca-year}. Leave empty if you want to display default car title.", MULTILISTING_DOMAIN) ?>
							</p>
						</div>
						<div class="separate_paragraph">
							<p>
								<input type="checkbox" id="pre_moderation" v-model="modal.pre_moderation">
								<label for="pre_moderation"><?php _e("Pre-moderation listing", MULTILISTING_DOMAIN) ?></label>
							</p>
						</div>
					</div>
					<div class="modal-footer">
						<button
							@click="applyListingSettings"
							type="button"
							class="button button-primary">
							<?php _e('Apply', MULTILISTING_DOMAIN) ?>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

	<div class="wrap_description">
		<div class="wrap_description_inner">
			<p>
				<strong>Label</strong> -
				<span>The title of your post type, which will be displayed in the admin part
					and on the general page for adding a listing</span>
			</p>
			<p>
				<strong>Slug</strong> - <span>Unique identifier to be used in the URL</span>
			</p>
			<p>
				<strong>Page add listing</strong> - <span>A unique page for adding a listing,
					it is important that each listing has its own. Will appear as a
					link on the "Add Listing Endpoint" page</span>
			</p>
			<p>
				<strong>Icon</strong> - <span>The class that identifies a specific icon can
					be used by Font Awesome or others defined in the Motors theme</span>
			</p>
		</div>
	</div>
</div>

<script>

</script>

