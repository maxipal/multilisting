<?php
if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

function stm_vc_location_valid($name) {
	$location = '';
	if(file_exists(get_stylesheet_directory() . "/vc_templates/{$name}.php")) {
		$location = get_stylesheet_directory() . "/vc_templates/{$name}.php";
	}else if(file_exists(get_template_directory() . "/vc_templates/{$name}.php")){
		$location = get_template_directory() . "/vc_templates/{$name}.php";
	}else{
		$location = MULTILISTING_PATH . "/templates/{$name}.php";
	}
	return $location;
}


add_action('init', 'stm_multilisting_vc_elements');
function stm_multilisting_vc_elements()
{
	if (function_exists('vc_add_params') && class_exists('STMMultiListing')) {
		$listings = STMMultiListing::getListings();
		$settings = STMMultiListing::getListingsSettings();
		if(!isset($settings['disable_orig_listing']) || $settings['disable_orig_listing'] == 'false'){
			$listings[] = array(
				'slug' => 'listings',
				'name' => __("Listings", MULTILISTING_DOMAIN)
			);
		}
		$post_types = $filter_options = [];

		if(!empty($listings)){
			$stm_filter_options = array();
			foreach ($listings as $listing){
				$post_types[$listing['name']] = $listing['slug'];

				if (function_exists('stm_get_listings_filter')) {
					if($listing['slug'] == 'listings')
						$filter_options = stm_get_car_filter();
					else
						$filter_options = stm_get_listings_filter($listing);
				}
				if (!empty($filter_options)) {
					foreach ($filter_options as $filter_option) {
						$key = $filter_option['single_name'] . ' (' . $filter_option['slug'] . ')';
						$stm_filter_options[$key] = $filter_option['slug'];
					}
				}
			}
		}

		vc_map([
			'name' => __('STM Multilisting Select Listing', 'motors'),
			'base' => 'stm_multilisting_add',
			'icon' => 'stm_multilisting_add',
			'category' => __('MultiListing', 'motors'),
			'html_template' => stm_vc_location_valid('stm_multilisting_add'),
			'params' => [
				array(
					'type' => 'css_editor',
					'heading' => __('Css', 'motors'),
					'param_name' => 'css',
					'group' => __('Design options', 'motors')
				)
			]
		]);

		vc_map([
			'name' => __('STM Multilisting Filter', 'motors'),
			'base' => 'stm_multilisting_filter',
			'icon' => 'stm_classic_filter',
			'category' => __('MultiListing', 'motors'),
			'html_template' => stm_vc_location_valid('stm_multilisting_filter'),
			'params' => [
				array(
					'type' => 'dropdown',
					'heading' => __('Select Post Type', 'motors'),
					'param_name' => 'post_type',
					'value' => $post_types,
				),
				array(
					'type' => 'css_editor',
					'heading' => __('Css', 'motors'),
					'param_name' => 'css',
					'group' => __('Design options', 'motors')
				)
			]
		]);


		vc_map([
			'name' => __("STM Add a Listing", MULTILISTING_DOMAIN),
			'base' => "stm_add_a_listing",
			'icon' => "stm_add_a_listing",
			'category' => __('MultiListing', MULTILISTING_DOMAIN),
			'html_template' => stm_vc_location_valid('stm_add_a_listing'),
			'params' => [
				[
					'type' => 'dropdown',
					'heading' => __('Listing to show', 'motors'),
					'param_name' => 'post_type',
					'value' => $post_types,
				],
				[
					'type' => 'dropdown',
					'heading' => __('Include listing title', 'motors'),
					'param_name' => 'show_car_title',
					'std' => 'no',
					'value' => [
						esc_html__('Yes', 'motors') => 'yes',
						esc_html__('No', 'motors') => 'no'
					],
				],
				[
					'type' => 'stm_autocomplete_vc_tax_multilist',
					'heading' => __('Main taxonomies to fill', 'motors'),
					'param_name' => 'taxonomy',
					'description' => __('Type slug of the category (don\'t delete anything from autocompleted suggestions)',
						'motors')
				],
				[
					'type' => 'checkbox',
					'heading' => __('Show number fields as input instead of dropdown',
						'motors'),
					'param_name' => 'use_inputs',
					'value' => [
						__('Yes', 'motors') => 'yes',
					],
				],
				[
					'type' => 'textfield',
					'heading' => __('Allowed histories', 'motors'),
					'param_name' => 'stm_histories',
					'description' => esc_html__('Enter allowed histories, separated by comma without spaces. Example - (Carfax, AutoCheck, Carfax 1 Owner, etc)',
						'motors'),
				],
				[
					'type' => 'param_group',
					'heading' => __('Items', 'motors'),
					'param_name' => 'items',
					'value' => urlencode(json_encode([
						[
							'label' => __('Listing feature title', 'motors'),
							'value' => '',
						],
						[
							'label' => __('Listing features', 'motors'),
							'value' => '',
						],
					])),
					'params' => [
						[
							'type' => 'textfield',
							'heading' => __('Listing feature section title', 'motors'),
							'param_name' => 'tab_title_single',
							'admin_label' => true,
						],
						[
							'type' => 'textfield',
							'heading' => __('Listing feature section features', 'motors'),
							'param_name' => 'tab_title_labels',
							'description' => esc_html__('Enter features, separated by comma without spaces. Example - (Bluetooth,DVD Player,etc)',
								'motors')
						],
					],
					'group' => esc_html__('Step 2 features', 'motors')
				],
				[
					'type' => 'textarea_html',
					'heading' => __('Media gallery notification text', 'motors'),
					'param_name' => 'content',
					'group' => esc_html__('Step 3 gallery', 'motors')
				],
				[
					'type' => 'textfield',
					'heading' => __('Seller template phrases', 'motors'),
					'param_name' => 'stm_phrases',
					'description' => esc_html__('Enter phrases, separated by comma without spaces. Example - (Excellent condition, Always garaged, etc)',
						'motors'),
					'group' => esc_html__('Step 4 phrases', 'motors')
				],
				[
					'type' => 'textfield',
					'heading' => __('Title for new users', 'motors'),
					'param_name' => 'stm_title_user',
					'group' => esc_html__('Register/Login User', 'motors')
				],
				[
					'type' => 'textarea',
					'heading' => __('Text for new users', 'motors'),
					'param_name' => 'stm_text_user',
					'group' => esc_html__('Register/Login User', 'motors')
				],
				[
					'type' => 'vc_link',
					'heading' => __('Agreement page', 'motors'),
					'param_name' => 'link',
					'group' => esc_html__('Register/Login User', 'motors')
				],
				[
					'type' => 'textfield',
					'heading' => __('Price title', 'motors'),
					'param_name' => 'stm_title_price',
					'group' => esc_html__('Price', 'motors')
				],
				[
					'type' => 'dropdown',
					'heading' => __('Show Price label', 'motors'),
					'param_name' => 'show_price_label',
					'group' => esc_html__('Price', 'motors'),
					'std' => 'no',
					'value' => [
						esc_html__('Yes', 'motors') => 'yes',
						esc_html__('No', 'motors') => 'no'
					],
				],
				[
					'type' => 'textarea',
					'heading' => __('Price description', 'motors'),
					'param_name' => 'stm_title_desc',
					'group' => esc_html__('Price', 'motors'),
				],
				[
					'type' => 'css_editor',
					'heading' => __('Css', 'motors'),
					'param_name' => 'css',
					'group' => __('Design options', 'motors')
				]
			]
		]);

		vc_map(array(
			'name' => __('STM Compare Multilisting', 'motors'),
			'base' => 'stm_compare_multilisting',
			'category' => __('MultiListing', 'motors'),
			'html_template' => stm_vc_location_valid('stm_compare_multilisting'),
			'params' => array(
				array(
					'type' => 'css_editor',
					'heading' => __('Css', 'motors'),
					'param_name' => 'css',
					'group' => __('Design options', 'motors')
				)
			)
		));

		vc_map(array(
			'name' => __('STM Multi-Listing Search (tabs)', 'motors'),
			'base' => 'stm_multilisting_search',
			'icon' => 'stm_multilisting_search',
			'category' => __('MultiListing', 'motors'),
			'html_template' => stm_vc_location_valid('stm_multilisting_search'),
			'params' => array(
				array(
					'type' => 'checkbox',
					'heading' => __('Show All', 'motors'),
					'param_name' => 'show_all',
					'value' => array(
						__('Yes', 'motors') => 'yes',
					),
					'std' => 'yes'
				),
				array(
					'type' => 'checkbox',
					'heading' => __('Show Category Listings amount', 'motors'),
					'param_name' => 'show_amount',
					'value' => array(
						__('Yes', 'motors') => 'yes',
					),
					'std' => 'yes'
				),
				array(
					'type' => 'textfield',
					'heading' => __('All tab label', 'motors'),
					'param_name' => 'show_all_label',
					'std' => __('All conditions', 'motors'),
					'dependency' => array('element' => 'show_all', 'value' => 'yes'),
				),
				array(
					'type' => 'textfield',
					'heading' => __('Search button postfix', 'motors'),
					'param_name' => 'search_button_postfix',
					'std' => __('Cars', 'motors'),
				),
				array(
					'type' => 'checkbox',
					'heading' => __('Select Taxonomies, which will be in this tab as filter', 'motors'),
					'param_name' => 'filter_all',
					'value' => $stm_filter_options,
					'dependency' => array('element' => 'show_all', 'value' => 'yes'),
				),
				array(
					'type' => 'textfield',
					'heading' => __('Select prefix', 'motors'),
					'param_name' => 'select_prefix',
				),
				array(
					'type' => 'textfield',
					'heading' => __('Select affix', 'motors'),
					'param_name' => 'select_affix',
				),
				array(
					'type' => 'textfield',
					'heading' => __('Number Select prefix', 'motors'),
					'param_name' => 'number_prefix',
				),
				array(
					'type' => 'textfield',
					'heading' => __('Number Select affix', 'motors'),
					'param_name' => 'number_affix',
				),
				array(
					'type' => 'param_group',
					'heading' => __('Items', 'motors'),
					'param_name' => 'items',
					'description' => __('Enter values for items - title, sub title.', 'motors'),
					'value' => urlencode(json_encode(array(
						array(
							'label' => __('Taxonomy', 'motors'),
							'value' => '',
						),
						array(
							'label' => __('Listing Type', 'motors'),
							'value' => '',
						),
						array(
							'label' => __('Tab Title', 'motors'),
							'value' => '',
						),
						array(
							'label' => __('Tab ID', 'motors'),
							'value' => '',
						),
						array(
							'label' => __('Filters', 'motors'),
							'value' => '',
						),
					))),
					'params' => array(
						array(
							'type' => 'stm_autocomplete_vc_multilist',//'stm_autocomplete_vc_tax_multilist',
							'heading' => __('Taxonomy', 'motors'),
							'param_name' => 'taxonomy_tab',
							'description' => __('Type slug of the category (don\'t delete anything from autocompleted suggestions. Note, only one taxonomy will be used as tab). This parameter will be used as default filter for this tab.', 'motors'),
						),
						array(
							'type' => 'dropdown',
							'heading' => __('Listing Type', 'motors'),
							'param_name' => 'tab_listing_type',
							'admin_label' => true,
							'value' => $post_types,
						),
						array(
							'type' => 'textfield',
							'heading' => __('Tab title', 'motors'),
							'param_name' => 'tab_title_single',
							'admin_label' => true,
						),
						array(
							'type' => 'textfield',
							'heading' => __('Tab ID', 'motors'),
							'param_name' => 'tab_id_single',
							'admin_label' => true,
						),
						array(
							'type' => 'checkbox',
							'heading' => __('Select Taxonomies, which will be in this tab as filter', 'motors'),
							'param_name' => 'filter_selected',
							'value' => $stm_filter_options
						),
					),
				),
				array(
					'type' => 'css_editor',
					'heading' => __('Css', 'motors'),
					'param_name' => 'css',
					'group' => __('Design options', 'motors')
				)
			)
		));

		vc_map(array(
			'name' => __('STM Multi-Listing tabs style 2', 'motors'),
			'base' => 'stm_multilistings_tabs_2',
			'icon' => 'stm_multilistings_tabs_2',
			'category' => __('MultiListing', 'motors'),
			'html_template' => stm_vc_location_valid('stm_multilistings_tabs_2'),
			'params' => array(
				array(
					'type' => 'textfield',
					'heading' => __('Title', 'motors'),
					'param_name' => 'title'
				),
				array(
					'type' => 'param_group',
					'heading' => __('Items', 'motors'),
					'param_name' => 'items',
					'description' => __('Enter values for listings - title, limit.', 'motors'),
					'value' => urlencode(json_encode(array(
						array(
							'label' => __('Tab Title', 'motors'),
							'value' => '',
						),
						array(
							'label' => __('Listing Type', 'motors'),
							'value' => '',
						),
						array(
							'label' => __('Number of listings to show in tab', 'motors'),
							'value' => '8',
						),
						array(
							'label' => __('Taxonomy', 'motors'),
							'value' => '',
						),
						array(
							'label' => __('Tab ID', 'motors'),
							'value' => '',
						),
					))),
					'params' => array(
						array(
							'type' => 'textfield',
							'heading' => __('Tab title', 'motors'),
							'param_name' => 'tab_title_single',
							'admin_label' => true,
						),
						array(
							'type' => 'dropdown',
							'heading' => __('Listing Type', 'motors'),
							'param_name' => 'tab_listing_type',
							'admin_label' => true,
							'value' => $post_types,
						),
						array(
							'type' => 'textfield',
							'heading' => __('Number of listings to show in tab', 'motors'),
							'param_name' => 'tab_limit',
						),
						array(
							'type' => 'stm_autocomplete_vc_multilist',
							'heading' => __('Taxonomy', 'motors'),
							'param_name' => 'taxonomy_tab',
							'description' => __('Type slug of the category (don\'t delete anything from autocompleted suggestions. Note, only one taxonomy will be used as tab). This parameter will be used as default filter for this tab.', 'motors'),
						),

						array(
							'type' => 'textfield',
							'heading' => __('Tab ID', 'motors'),
							'param_name' => 'tab_id_single',
						),
					),
				),
				array(
					'type' => 'checkbox',
					'heading' => __('Include featured items', 'motors'),
					'param_name' => 'featured',
					'value' => array(
						__('Yes', 'motors') => 'yes',
					),
					'std' => 'yes'
				),
				array(
					'type' => 'textfield',
					'heading' => __('Featured tabs label', 'motors'),
					'param_name' => 'featured_label',
					'std' => __('Featured items', 'motors'),
					'dependency' => array('element' => 'featured', 'value' => 'yes'),
				),
				array(
					'type' => 'checkbox',
					'heading' => __('Show "Show more" button in tabs', 'motors'),
					'param_name' => 'show_more',
					'value' => array(
						__('Yes', 'motors') => 'yes',
					),
					'std' => 'yes'
				),
				array(
					'type' => 'css_editor',
					'heading' => __('Css', 'motors'),
					'param_name' => 'css',
					'group' => __('Design options', 'motors')
				)
			)
		));

		vc_map(array(
			'name' => __('STM Icon Filter', 'motors'),
			'base' => 'stm_multi_icon_filter',
			'icon' => 'stm_multi_icon_filter',
			'category' => __('MultiListing', 'motors'),
			'html_template' => stm_vc_location_valid('stm_multi_icon_filter'),
			'params' => array(
				[
					'type' => 'dropdown',
					'heading' => __('Listing to show', 'motors'),
					'param_name' => 'post_type',
					'value' => $post_types,
				],
				[
					'type' => 'stm_autocomplete_vc_tax_multilist',
					'heading' => __('Main taxonomies to fill', 'motors'),
					'param_name' => 'filter_selected',
					'description' => __('Type slug of the category (don\'t delete anything from autocompleted suggestions)',
						'motors')
				],
				array(
					'type' => 'dropdown',
					'heading' => __('As Carousel', 'motors'),
					'param_name' => 'as_carousel',
					'value' => array(
						__('Yes', 'motors') => 'yes',
						__('No', 'motors') => 'no'
					),
					'std' => 'no'
				),
				array(
					'type' => 'textfield',
					'heading' => __('Limit', 'motors'),
					'param_name' => 'limit',
					'dependency' => array('element' => 'as_carousel', 'value' => 'no'),
				),
				array(
					'type' => 'dropdown',
					'heading' => __('Per row', 'motors'),
					'param_name' => 'per_row',
					'value' => array(
						'1' => 1,
						'2' => 2,
						'3' => 3,
						'4' => 4,
						'6' => 6,
						'9' => 9,
						'12' => 12
					),
					'std' => 4,
					'dependency' => array('element' => 'as_carousel', 'value' => 'no'),
				),
				array(
					'type' => 'dropdown',
					'heading' => __('Items Align', 'motors'),
					'param_name' => 'align',
					'value' => array(
						__('Left', 'motors') => 'left',
						__('Center', 'motors') => 'center',
						__('Right', 'motors') => 'right',
					),
					'std' => 'left',
					'dependency' => array('element' => 'as_carousel', 'value' => 'no'),
				),
				array(
					'type' => 'textarea_html',
					'heading' => __('Content', 'motors'),
					'param_name' => 'content'
				),
				array(
					'type' => 'textfield',
					'heading' => __('"Show all" label text', 'motors'),
					'param_name' => 'duration',
					'description' => __('If you want to show only important types, other will be hidden, till user click on this label', 'motors'),
					'value' => 'Show all',
					'dependency' => array('element' => 'as_carousel', 'value' => 'no'),
				),
				array(
					'type' => 'css_editor',
					'heading' => __('Css', 'motors'),
					'param_name' => 'css',
					'group' => __('Design options', 'motors')
				)
			)
		));

		vc_map(array(
			'name' => __('STM Carousel Listings', 'motors'),
			'base' => 'stm_carousel_listings',
			'icon' => 'stm_carousel_listings',
			'category' => __('MultiListing', 'motors'),
			'html_template' => stm_vc_location_valid('stm_carousel_listings'),
			'params' => array(
				[
					'type' => 'dropdown',
					'heading' => __('Listing to show', 'motors'),
					'param_name' => 'post_type',
					'value' => $post_types,
				],
//				[
//					'type' => 'stm_autocomplete_vc_tax_multilist',
//					'heading' => __('Main taxonomies to fill', 'motors'),
//					'param_name' => 'filter_selected',
//					'description' => __('Type slug of the category (don\'t delete anything from autocompleted suggestions)',
//						'motors')
//				],
				array(
					'type' => 'checkbox',
					'heading' => __('Autoplay Carousel', 'motors'),
					'param_name' => 'autoplay',
				),
				array(
					'type' => 'textfield',
					'heading' => __('Listings Limit', 'motors'),
					'param_name' => 'limit',
				),
				array(
					'type' => 'textfield',
					'heading' => __('Visibility Listing', 'motors'),
					'param_name' => 'vis_limit',
				),
//				array(
//					'type' => 'textfield',
//					'heading' => __('"Show all" label text', 'motors'),
//					'param_name' => 'duration',
//					'description' => __('If you want to show only important types, other will be hidden, till user click on this label', 'motors'),
//					'value' => 'Show all',
//				),
				array(
					'type' => 'css_editor',
					'heading' => __('Css', 'motors'),
					'param_name' => 'css',
					'group' => __('Design options', 'motors')
				)
			)
		));

	}
}


function stm_autocomplete_vc_st_tax_multilist( $settings, $value )
{
	if(!class_exists('STMMultiListing')) return false;
	$listings = STMMultiListing::getListings();
	$taxes = $temp = [];
	if(!empty($listings)){
		foreach ($listings as $listing){
			$slug = $listing['slug'];
			$temp = get_object_taxonomies( $slug, 'objects' );
			foreach ($temp as $item) {
				$taxes[$listing['name'].' - '.$item->label] = $item->name;
			}
		}
	}
	return '<div class="stm_autocomplete_vc_field">'
		.  '<script type="text/javascript">'
		.  'var st_vc_taxonomies = ' . json_encode( $taxes )
		.  '</script>'
		.  '<input 
				type="text" 
				name="' . esc_attr( $settings['param_name'] ) . '" 
				class="stm_autocomplete_vc wpb_vc_param_value wpb-textinput ' .
				esc_attr( $settings['param_name'] ) . ' ' .
				esc_attr( $settings['type'] ) . '_field" 
				value="' . esc_attr( $value ) . '" />' .
		'</div>';
}
if(function_exists('vc_add_shortcode_param')){
	vc_add_shortcode_param(
		'stm_autocomplete_vc_tax_multilist',
		'stm_autocomplete_vc_st_tax_multilist',
		MULTILISTING_PLUGIN_URL . '/assets/js/jquery-ui.min.js'
	);
}


function stm_autocomplete_vc_st_multilist( $settings, $value )
{
	if(!class_exists('STMMultiListing')) return false;
	$listings = STMMultiListing::getListings();
	$categories = array();

	if(!empty($listings)){
		foreach ($listings as $listing){
			$filter_options = get_option("stm_{$listing['slug']}_options");
			//Creating new array for tax query && meta query

			$terms_args = array(
				'orderby' => 'name',
				'order' => 'ASC',
				'hide_empty' => false,
				'fields' => 'all',
				'pad_counts' => false,
			);

			if (!empty($filter_options)) {
				foreach ($filter_options as $filter_option) {
					if (empty($filter_option['numeric'])) {

						$terms = get_terms($filter_option['slug'], $terms_args);
						foreach ($terms as $term) {
							$categories[$listing['slug']. '-' .$term->slug] = $term->slug . ' | ' . $filter_option['slug'];
						}
					}
				}
			}
		}
	}
	return '<div class="stm_autocomplete_vc_field">'
		.  '<script type="text/javascript">'
		.  'var st_vc_taxonomies = ' . json_encode( $categories )
		.  '</script>'
		.  '<input 
				type="text" 
				name="' . esc_attr( $settings['param_name'] ) . '" 
				class="stm_autocomplete_vc wpb_vc_param_value wpb-textinput ' .
		esc_attr( $settings['param_name'] ) . ' ' .
		esc_attr( $settings['type'] ) . '_field" 
				value="' . esc_attr( $value ) . '" />' .
		'</div>';
}
if(function_exists('vc_add_shortcode_param')) {
	vc_add_shortcode_param(
		'stm_autocomplete_vc_multilist',
		'stm_autocomplete_vc_st_multilist',
		MULTILISTING_PLUGIN_URL . '/assets/js/jquery-ui.min.js'
	);
}

if (class_exists('WPBakeryShortCodesContainer')) {
	class WPBakeryShortCode_STM_Multilisting_Filter extends WPBakeryShortCode{}
	class WPBakeryShortCode_STM_Multilisting_Add extends WPBakeryShortCode{}
	class WPBakeryShortCode_STM_Add_A_Listing extends WPBakeryShortCode{}
	class WPBakeryShortCode_STM_Compare_Multilisting extends WPBakeryShortCode{}
	class WPBakeryShortCode_STM_Multilisting_Search extends WPBakeryShortCode{}
	class WPBakeryShortCode_STM_Multilisting_Tabs_2 extends WPBakeryShortCode{}
	class WPBakeryShortCode_STM_Multi_Icon_Filter extends WPBakeryShortCode{}
}
