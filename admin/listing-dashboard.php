<?php

function stm_get_listings_filter($listing) {
	$args = wp_parse_args(array('use_on_car_filter' => true), array(
		'where' => array(),
		'key_by' => ''
	));

	$result = array();
	$data = array_filter((array)get_option("stm_{$listing['slug']}_options"));

	foreach ($data as $key => $_data) {
		$passed = true;
		foreach ($args['where'] as $_field => $_val) {
			if (array_key_exists($_field, $_data) && $_data[$_field] != $_val) {
				$passed = false;
				break;
			}
		}

		if ($passed) {
			if ($args['key_by']) {
				$result[$_data[$args['key_by']]] = $_data;
			} else {
				$result[] = $_data;
			}
		}
	}
	return apply_filters('stm_listings_attributes', $result, $args);
}


add_filter('stm_listings_ajax_results', 'multilisting_remove_posttype_url',100);
function multilisting_remove_posttype_url($args) {
	$args['url'] = remove_query_arg( array( 'posttype', 'ajax_action', 'fragments' ) );
	return $args;
}

add_action( 'admin_init', 'theme_options_init' );
function theme_options_init(){
	register_setting( 'theme_options_multilisting_motors', 'wpuniq_theme_multilisting_motors');
}

add_action( 'wp_enqueue_scripts', 'multilisting_front_enqueue_scripts_styles' );
function multilisting_front_enqueue_scripts_styles(){
	wp_enqueue_script(
		'multilisting-frontend',
		MULTILISTING_PLUGIN_URL . '/assets/js/frontend.custom.js',
		['listings-filter'],
		null,
		true
	);
}

add_action( 'admin_enqueue_scripts', 'multilisting_admin_enqueue_scripts_styles' );
function multilisting_admin_enqueue_scripts_styles(){
	wp_enqueue_style( 'css3-tabs', MULTILISTING_PLUGIN_URL . '/assets/css/tabs.css', NULL, NULL, 'all' );
	wp_enqueue_style( 'listing-frontend', MULTILISTING_PLUGIN_URL . '/assets/css/listing.css', NULL, NULL, 'all' );
	wp_enqueue_script( 'vue', MULTILISTING_PLUGIN_URL . '/assets/js/vue.2.6.11.js', NULL, NULL, true );
	wp_enqueue_script(
		'listing-frontend',
		MULTILISTING_PLUGIN_URL . '/assets/js/listing.js',
		array('vue', 'jquery','jquery-ui-droppable', 'jquery-ui-datepicker', 'jquery-ui-sortable'),
		'1.0.1',
		true
	);

	wp_enqueue_script( 'jquery-ui-sortable' );

	wp_enqueue_script(
		'stm-theme-multiselect',
		STM_LISTINGS_URL . '/assets/js/jquery.multi-select.js',
		array('jquery')
	);
	wp_enqueue_script(
		'stm-listings-js',
		STM_LISTINGS_URL . '/assets/js/vehicles-listing.js',
		array('jquery','jquery-ui-droppable', 'jquery-ui-datepicker', 'jquery-ui-sortable'),
		'6.5.8.1'
	);


	$current_listing = '';
	$listings = get_option('stm_multilisting_listings', []);
	$settings = get_option('stm_multilisting_settings', []);
	if(!empty($_REQUEST['post_type'])){
		if(!empty($listings)){
			foreach ($listings as $listing) {
				if($listing['slug'] == $_REQUEST['post_type']){
					$current_listing = $listing['slug'];
					break;
				}
			}
		}
	}
	$pages = [__("Select add listing page", MULTILISTING_DOMAIN)];
	$p_temp = get_posts([
		'post_type' => 'page',
		'posts_per_page' => -1,
		'post_status' => 'publish'
	]);
	if(!empty($p_temp)){
		foreach ($p_temp as $item) {
			$pages[$item->ID] = $item->post_title;
		}
	}
	if(!empty($settings['disable_orig_listing']) && $settings['disable_orig_listing'] == 'true'){
		$settings['disable_orig_listing'] = true;
	}else{
		$settings['disable_orig_listing'] = false;
	}
	wp_localize_script( 'listing-frontend', 'stm_listings', array(
		'listings' => $listings,
		'settings' => $settings,
		'c_listing' => $current_listing,
		'pages' => $pages,
	) );
}

add_action('wp_ajax_save_multilisting_data', 'save_multilisting_data');
function save_multilisting_data() {
	$listings = $_REQUEST['listings'];
	$settings = $_REQUEST['settings'];

	if(is_array($listings) && count($listings)){
		foreach ($listings as $key => $listing) {
			if(empty($listing['name']) || empty($listing['slug']))
				unset($listings[$key]);
		}
	}

	// Set endpoint on add a car page
	set_theme_mod(
		'header_listing_btn_link',
		get_bloginfo('home')."/".HooksMultiListing::getAddListingName()
	);

	update_option('stm_multilisting_listings', $listings);
	update_option('stm_multilisting_settings', $settings);
	flush_rewrite_rules();
}

function stm_get_single_car_multilisting($post_type) {
	$args = array(
		'where' => array('use_on_single_car_page' => true),
		'key_by' => ''
	);

	$result = array();
	$data = array_filter((array)get_option("stm_{$post_type}_options"));

	foreach ($data as $key => $_data) {
		$passed = true;
		foreach ($args['where'] as $_field => $_val) {
			if (array_key_exists($_field, $_data) && $_data[$_field] != $_val) {
				$passed = false;
				break;
			}
		}

		if ($passed) {
			if ($args['key_by']) {
				$result[$_data[$args['key_by']]] = $_data;
			} else {
				$result[] = $_data;
			}
		}
	}

	return apply_filters('stm_listings_attributes', $result, $args);
}


function stm_get_custom_taxonomy_pt_count($slug, $taxonomy, $pt)
{
	// this is cached function, so we can use it
	$total = 0;

	$args = array(
		'post_type' => $pt,
		'post_status' => 'publish',
		'posts_per_page' => 1,
		'suppress_filters' => 0,
	);
	$meta = array(
		'relation' => 'AND',
		array(
			'relation' => 'OR',
			array(
				'key' => 'car_mark_as_sold',
				'value' => '',
				'compare'  => 'NOT EXISTS'
			),
			array(
				'key' => 'car_mark_as_sold',
				'value' => '',
				'compare'  => '='
			)
		)
	);

	if(!empty($slug) && !empty($taxonomy)){
		$args['tax_query'] = array(
			array(
				'taxonomy' => $taxonomy,
				'terms' => $slug,
				'field' => 'slug',
			)
		);
	}
	if(!empty($taxonomy) && is_array($taxonomy)){
		if($pt == 'listings')
			$filter_options = get_option('stm_vehicle_listing_options');
		else
			$filter_options = get_option("stm_{$pt}_options", []);
		foreach ($taxonomy as $tax){
			if (!empty($filter_options)) {
				$taxonomy_info = array();
				foreach ($filter_options as $filter_option) {
					if ($filter_option['slug'] == $tax) {
						$taxonomy_info = $filter_option;
					}
				}

				if ( !empty( $taxonomy_info['numeric'] ) and $taxonomy_info['numeric'] ) {
					$__args = array(
						'orderby' => 'name',
						'order' => 'ASC',
						'hide_empty' => false,
						'fields' => 'all',
					);
					$numbers = array();
					$terms = get_terms( $tax, $__args );

					if ( !empty( $terms ) ) {
						foreach ( $terms as $term ) {
							$numbers[] = intval( $term->name );
						}
					}
					sort( $numbers );

					$start_value = !empty(current($numbers)) ? current($numbers) : 0;
					$end_value = !empty(end($numbers)) ? end($numbers) : 0;

					$meta[1]['relation'] = 'AND';
					$meta[1][] = [
						'key'     => $tax,
						'value'   => $start_value,
						'compare' => '>=',
						'type' => 'NUMERIC'
					];
					$meta[1][] = [
						'key'     => $tax,
						'value'   => $end_value,
						'compare' => '<=',
						'type' => 'NUMERIC'
					];
					continue;
				}
			}
		}
	}
	$args['meta_query'] = $meta;

	$count_posts = new WP_Query($args);
	if (!is_wp_error($count_posts)) {
		$total = $count_posts->found_posts;
	}
	if(!empty($slug) && !empty($taxonomy)) {
		$total = wp_count_posts($pt);
		$total = $total->publish;
	}

	return $total;
}


if (!function_exists('stm_user_listings_query')) {
	/**
	 * Get User cars
	 *
	 * @param $user_id
	 * @param string $status
	 * @param int $per_page
	 * @param bool $popular
	 * @param int $offset
	 * @param bool $data_desc
	 *
	 * @return WP_Query
	 */
	function stm_user_listings_query($user_id, $status = "publish", $per_page = -1, $popular = false, $offset = 0, $data_desc = false, $getAll = false)
	{
		$ppl = ($getAll) ? array() : array( 'key' => 'pay_per_listing', 'compare' => 'NOT EXISTS', 'value' => '' );

		$post_types = [];
		if(is_author(get_current_user_id())){
			if(!empty($_REQUEST['view-myself']) || !is_user_logged_in()){
				$listings = STMMultiListing::getListings();
				if(!empty($listings)){
					foreach ($listings as $listing) {
						$post_types[] = $listing['slug'];
					}
				}
			}
		}
		$settings = STMMultiListing::getListingsSettings();
		if(!isset($settings['disable_orig_listing']) || $settings['disable_orig_listing'] == 'false'){
			$post_types[] = 'listings';
		}
		$args = array(
			'post_type' => $post_types,
			'post_status' => $status,
			'posts_per_page' => $per_page,
			'offset' => $offset,
			'author' => $user_id,
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => 'stm_car_user',
					'value' => $user_id,
					'compare' => '='
				),
				$ppl
			)
		);

		if ($popular) {
			$args['order'] = 'ASC';
			$args['orderby'] = 'stm_car_views';
		}

		$query = new WP_Query($args);
		wp_reset_postdata();

		return $query;

	}
}

function stm_user_multilistings_query(
	$user_id,
	$status = "publish",
	$post_type = 'listings',
	$per_page = -1,
	$popular = false,
	$offset = 0,
	$data_desc = false,
	$getAll = false)
{
	$ppl = ($getAll) ? array() : array( 'key' => 'pay_per_listing', 'compare' => 'NOT EXISTS', 'value' => '' );

	$args = array(
		'post_type' => $post_type,
		'post_status' => $status,
		'posts_per_page' => $per_page,
		'offset' => $offset,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'stm_car_user',
				'value' => $user_id,
				'compare' => '='
			),
			$ppl
		)
	);

	if ($popular) {
		$args['order'] = 'ASC';
		$args['orderby'] = 'stm_car_views';
	}

	$query = new WP_Query($args);
	wp_reset_postdata();

	return $query;
}

function stm_user_pay_per_multilistings_query(
	$user_id,
	$status = "publish",
	$post_type = 'listings',
	$per_page = -1,
	$popular = false,
	$offset = 0,
	$data_desc = false)
{
	$args = array(
		'post_type' => $post_type,
		'post_status' => $status,
		'posts_per_page' => $per_page,
		'offset' => $offset,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'stm_car_user',
				'value' => $user_id,
				'compare' => '='
			),
			array(
				'key' => 'pay_per_listing',
				'compare' => '=',
				'value' => 'pay'
			)
		)
	);

	if ($popular) {
		$args['order'] = 'ASC';
		$args['orderby'] = 'stm_car_views';
	}

	$query = new WP_Query($args);
	wp_reset_postdata();

	return $query;

}



add_action( 'admin_menu', 'multi_listing_admin_page' );
function multi_listing_admin_page() {
	add_menu_page(
		__( 'Multi-Listing',MULTILISTING_DOMAIN ),
		__( 'Multi-Listing',MULTILISTING_DOMAIN ),
		'manage_options',
		'multi-listing',
		'multi_listing_callback',
		'dashicons-index-card',
		25
	);
}
function multi_listing_callback(){

	if(!isset($_REQUEST['settings-updated'])) $_REQUEST['settings-updated'] = false;?>

	<div class="wrap" id="listing_list_app" ref="application">
		<h2><?php _e("Multi-Listing Dashboard",MULTILISTING_DOMAIN); ?></h2>
		<div id="message" class="updated" v-show="successSaved">
			<p><strong><?php _e( 'Settings saved',MULTILISTING_DOMAIN ); ?></strong></p>
		</div>
		<form method="post" action="options.php">
			<?php settings_fields( 'theme_options_multilisting_motors' ); ?>
			<?php $options = get_option( 'wpuniq_theme_multilisting_motors' );?>

			<div class="tabs">
				<input id="tab1" type="radio" name="tabs" checked>
				<label
					for="tab1"
					title="<?php _e("Listings",MULTILISTING_DOMAIN) ?>">
					<i class="fa fa-cog"></i>
					<?php _e("Listings",MULTILISTING_DOMAIN) ?>
				</label>

				<input id="tab2" type="radio" name="tabs">
				<label
					for="tab2"
					title="<?php _e("Settings",MULTILISTING_DOMAIN) ?>">
					<i class="fa fa-search"></i>
					<?php _e("Settings",MULTILISTING_DOMAIN) ?>
				</label>

				<?php if(false): ?>
					<input id="tab3" type="radio" name="tabs">
					<label
						for="tab3"
						title="<?php _e("Messages",MULTILISTING_DOMAIN) ?>">
						<i class="fa fa-map-marker"></i>
						<?php _e("Messages",MULTILISTING_DOMAIN) ?>
					</label>
				<?php endif ?>

				<section id="content1" class="clearfix">
					<?php include(MULTILISTING_PATH."/admin/listing-list.php") ?>
				</section>

				<section id="content2" class="clearfix">
					<?php include(MULTILISTING_PATH."/admin/settings.php") ?>
				</section>

				<?php if(false): ?>
					<section id="content3" class="clearfix">
						<?php include(MULTILISTING_PATH."/pages/messages.php") ?>
					</section>
				<?php endif ?>

				<input type="hidden" name="saveOpt" value="true">
			</div>

			<?php do_settings_sections( 'theme_options_multilisting_motors' ); ?>
			<p class="submit">
				<input
					type="button"
					@click="submit(event)"
					value="<?php _e("Save Changes") ?>"
					class="button button-primary">
			</p>
		</form>
	</div>
	<?php
}


if ( !function_exists( 'stm_generate_title_from_slugs' ) ) {
	function stm_generate_title_from_slugs( $post_id, $show_labels = false )
	{
		$title_from = get_theme_mod( 'listing_directory_title_frontend', '' );
		$listings_type = get_post_type( $post_id );
		$multilisting = new STMMultiListing();
		$ml_title_from = $multilisting->getListingSetting('generate_title', $listings_type);
		$listings = $multilisting->listListingSlugs();
		$listings[] = 'listings';
		if(!empty($ml_title_from)){
			$title_from = $ml_title_from;
		}

		$title_return = '';
		if ( in_array($listings_type, $listings) ) {

			if ( !empty( $title_from ) and is_listing() || stm_is_car_dealer() || stm_is_dealer_two() || stm_is_aircrafts() ) {
				$title = stm_replace_curly_brackets( $title_from );
				$title_counter = 0;

				if ( !empty( $title ) ) {
					foreach ( $title as $title_part ) {
						$title_counter++;
						if ( $title_counter == 1 ) {
							if ( $show_labels ) {
								$title_return .= '<div class="labels">';
							}
						}

						$term = wp_get_post_terms( $post_id, strtolower( $title_part ), array( 'orderby' => 'none' ) );
						if ( !empty( $term ) and !is_wp_error( $term ) ) {
							if ( !empty( $term[0] ) ) {
								if ( !empty( $term[0]->name ) ) {
									if ( $title_counter == 1 ) {
										$title_return .= $term[0]->name;
									} else {
										$title_return .= ' ' . $term[0]->name;
									}
								} else {
									$number_affix = get_post_meta( $post_id, strtolower( $title_part ), true );
									if ( !empty( $number_affix ) ) {
										$title_return .= ' ' . $number_affix . ' ';
									}
								}
							}
						} else {
							$number_affix = get_post_meta( $post_id, strtolower( $title_part ), true );
							if ( !empty( $number_affix ) ) {
								$title_return .= ' ' . $number_affix . ' ';
							}
						}
						if ( $show_labels and $title_counter == 2 ) {
							$title_return .= '</div>';
						}
					}
				}
			} elseif ( !empty( $title_from ) and stm_is_boats() ) {
				$title = stm_replace_curly_brackets( $title_from );

				if ( !empty( $title ) ) {
					foreach ( $title as $title_part ) {
						$value = get_post_meta( $post_id, $title_part, true );
						if ( !empty( $value ) ) {
							$cat = get_term_by( 'slug', $value, $title_part );
							if ( !is_wp_error( $cat ) and !empty( $cat->name ) ) {
								$title_return .= $cat->name . ' ';
							} else {
								$title_return .= $value . ' ';
							}
						}
					}
				}
			} elseif ( !empty( $title_from ) and stm_is_motorcycle()) {
				$title = stm_replace_curly_brackets( $title_from );

				$title_counter = 0;

				if ( !empty( $title ) ) {
					foreach ( $title as $title_part ) {
						$value = get_post_meta( $post_id, $title_part, true );
						$title_counter++;

						if ( !empty( $value ) ) {
							$cat = get_term_by( 'slug', $value, $title_part );
							if ( !is_wp_error( $cat ) and !empty( $cat->name ) ) {
								if ( $title_counter == 1 and $show_labels ) {
									$title_return .= '<span class="stm-label-title">';
								}
								$title_return .= $cat->name . ' ';
								if ( $title_counter == 1 and $show_labels ) {
									$title_return .= '</span>';
								}
							} else {
								if ( $title_counter == 1 and $show_labels ) {
									$title_return .= '<span class="stm-label-title">';
								}
								$title_return .= $value . ' ';
								if ( $title_counter == 1 and $show_labels ) {
									$title_return .= '</span>';
								}
							}
						}
					}
				}
			} elseif ( !empty( $title_from ) && stm_is_listing_three()) {
				$title = stm_replace_curly_brackets( $title_from );
				$title_counter = 0;

				if ( !empty( $title ) ) {
					foreach ( $title as $title_part ) {
						$title_counter++;
						if ( $title_counter == 1 ) {
							if ( $show_labels ) {
								$title_return .= '<div class="labels">';
							}
						}

						$term = wp_get_post_terms( $post_id, strtolower( $title_part ), array( 'orderby' => 'none' ) );
						if ( !empty( $term ) and !is_wp_error( $term ) ) {
							if ( !empty( $term[0] ) ) {
								if ( !empty( $term[0]->name ) ) {
									if ( $title_counter == 1 ) {
										$title_return .= $term[0]->name;
									} else {
										$title_return .= ' ' . $term[0]->name;
									}
								} else {
									$number_affix = get_post_meta( $post_id, strtolower( $title_part ), true );
									if ( !empty( $number_affix ) ) {
										$title_return .= ' ' . $number_affix . ' ';
									}
								}
							}
						} else {
							$number_affix = get_post_meta( $post_id, strtolower( $title_part ), true );
							if ( !empty( $number_affix ) ) {
								$title_return .= ' ' . $number_affix . ' ';
							}
						}
						if ( $show_labels and $title_counter == 2 ) {
							$title_return .= '</div>';
						}
					}
				}
			} elseif ( !empty( $title_from ) && stm_is_equipment()) {
				$title = stm_replace_curly_brackets( $title_from );
				$title_counter = 0;

				if ( !empty( $title ) ) {
					foreach ( $title as $title_part ) {
						$title_counter++;
						if ( $title_counter == 1 ) {
							if ( $show_labels ) {
								$title_return .= '<div class="labels">';
							}
						}

						$term = wp_get_post_terms( $post_id, strtolower( $title_part ), array( 'orderby' => 'none' ) );

						if ( !empty( $term ) and !is_wp_error( $term ) ) {

							if ( !empty( $term[0] ) ) {

								if ( !empty( $term[0]->name ) ) {

									if ( $title_counter == 1 ) {
										$title_return .= $term[0]->name;
									} else {
										$title_return .= ' ' . $term[0]->name;
									}
								} else {
									$number_affix = get_post_meta( $post_id, strtolower( $title_part ), true );
									if ( !empty( $number_affix ) ) {
										$title_return .= ' ' . $number_affix . ' ';
									}
								}
							}
						} else {
							$number_affix = get_post_meta( $post_id, strtolower( $title_part ), true );
							if ( !empty( $number_affix ) ) {
								$title_return .= ' ' . $number_affix . ' ';
							}
						}

						if ( $show_labels and $title_counter == 2 ) {
							$title_return .= '</div>';
						}
					}
				}
			}
		}

		if ( empty( $title_return ) ) {
			$title_return = get_the_title( $post_id );
		}

		return $title_return;
	}
}
