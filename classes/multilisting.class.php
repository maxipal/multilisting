<?php
if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class STMMultiListing {
	protected $listings = [];
	protected $settings = [];
	public function __construct() {
		$this->listings = get_option('stm_multilisting_listings', []);
		$this->settings = get_option('stm_multilisting_settings', []);
		add_action( 'after_setup_theme', array($this, 'multilisting_load_theme_textdomain') );
		add_action('admin_enqueue_scripts', array($this, 'admin_enqueue'), 100);
	}

	public function admin_enqueue() {
		wp_enqueue_style('font-awesome', STM_LISTINGS_URL . '/assets/css/font-awesome.min.css', array());
	}
	static public function getListings() {
		return get_option('stm_multilisting_listings', []);
	}

	static public function getListingsSettings() {
		return get_option('stm_multilisting_settings', []);
	}

	function multilisting_load_theme_textdomain() {
		load_theme_textdomain( MULTILISTING_DOMAIN, MULTILISTING_PLUGIN_URL . '/languages' );
	}


	public function getCurrentListing() {
		if(is_admin()) return false;
		global $wp_query;
		if(!$wp_query) return false;
		if(!empty($_REQUEST['posttype'])) $post_type = esc_html($_REQUEST['posttype']);// In Inventory page request
		else{
			$post_type = !empty($wp_query->get('listings_type')) ? $wp_query->get('listings_type') : get_post_type();
		}
		if($post_type){
			if(!empty($this->listings)){
				foreach ($this->listings as $listing) {
					if($post_type == $listing['slug'])
						return $listing;
				}
			}
		}
		return false;
	}

	public function getListingNameBySlug($slug) {
		if(!empty($this->listings)){
			foreach ($this->listings as $listing) {
				if($listing['slug'] == $slug) return $listing['name'];
			}
		}
		return '';
	}

	public function registerPostTypes() {
		if(is_array($this->listings) && count($this->listings)){
			foreach ($this->listings as $listing) {
				if(empty($listing['slug']) || empty($listing['name'])) return;
				if(post_type_exists($listing['slug'])) continue;

				$this->registerListings($listing);
			}
		}
	}

	protected function savePostTypeOptions($listing, $options)	{
		$settings = stm_listings_page_options();

		foreach ($options as $key => $option) {
			foreach ($option as $name => $item) {
				if ($settings[$name]['type'] == 'checkbox' and !empty($item) and $item) {
					$options[$key][$name] = 1;
				}
			}
		}

		if(current_user_can('administrator') || current_user_can('editor')) {
			update_option( "stm_{$listing['slug']}_options", $options );
		}
	}

	public function listListingSlugs() {
		$temp = [];
		if(!empty($this->listings)){
			foreach ($this->listings as $listing) {
				$temp[] = $listing['slug'];
			}
		}
		return $temp;
	}

	public function getListingSetting($setting_name, $listing_slug){
		foreach ($this->listings as $listing) {
			if ($listing['slug'] == $listing_slug) {
				if (!empty($listing['settings'][$setting_name])) {
					return $listing['settings'][$setting_name];
				}
			}
		}
		return false;
	}

}

new STMMultiListing;
