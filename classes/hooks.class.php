<?php
if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


class HooksMultiListing extends STMMultiListing
{

	public $add_listing_name = '';
	public function __construct()
	{
		parent::__construct();

		add_action( 'template_redirect', array($this, 'STMListingsAttributesFilter'), 100 );
		add_filter( 'stm_listings_filter', array($this, 'STMListingsFilter'), 11, 2 );
		add_filter( 'stm_listings_build_query_args', array($this, 'queryArgsPostType' ), 10, 2 );
		add_action( 'stm_listings_filter_after', array($this, 'addPostTypeSidebar'), 100 );
		add_filter( 'stm_account_navigation', array($this, 'addPostTypesUserProfileNavigation') );
		add_filter( 'option_stm_vehicle_listing_options', array($this, 'replaceVehicleListingOption'),11,2);

		// Add permalink rule for add listing page
		add_action( 'init', array($this, 'addPostTypeSelectRule') );
		add_filter( 'query_vars', array($this, 'addPostTypeSelectWhiteList') );
		add_action( 'template_include', array($this, 'addPostTypeSelect') );

		add_action('init', array($this, 'registerPostTypes'), 11);
		add_action( 'init', array($this, 'deregisterOriginalPostType'), 1000 );
//		add_filter( 'stm_listings_post_type', array($this, 'extendedListingPostType'));

		add_action( 'wp_enqueue_scripts', array($this, 'loadFrontEnqueueScriptsStyles') );
		add_filter( 'stm_listing_save_post_data', array($this, 'addPostTypeOnInsertPost') );

		$add_listing_name = !empty($this->settings['add_listing_name']) ? $this->settings['add_listing_name'] : 'select-listing';
		$this->add_listing_name = apply_filters('multilisting_add_listing_name', $add_listing_name);

		// Replace messages and profile page for listings
		add_filter( 'stm_filter_add_a_car', array($this, 'replaceNotifyOnAddListing') );
		add_filter( 'stm_filter_add_car_media', array($this, 'replaceNotifyOnAddListingMedia') );

		add_filter( 'stm_similar_cars_query', array($this, 'similarListingQuery'));

		add_action( 'pre_get_posts', array($this, 'replaceListingOnFavoritePage') );
		add_filter( 'gettext', array($this, 'changeTranslateTextMultiple'), 20 );
		add_filter( 'stm_filter_listing_link', array($this, 'stm_filter_listing_link'), 20, 2 );
		add_filter( 'stm_listings_binding_results', array($this, 'stm_binding_results_return_200'));

		add_action( 'woocommerce_checkout_update_order_meta', 'stm_before_create_order', 200, 2 );
		add_action( 'init', array($this, 'stm_register_perpay') );

		add_filter('theme_mod_dealer_pay_per_listing', array($this, 'enable_pay_per_listing'));
		add_filter('stm_filter_user_restrictions', array($this, 'disable_restriction_post_ppl'),100);
		add_filter('theme_mod_pay_per_listing_price', array($this, 'pay_per_listing_price'));
		add_action('stm_after_listing_gallery_saved', array($this, 'stm_after_listing_gallery_saved'));
		add_filter('stm_listings_inventory_page_id', array($this, 'stm_listings_inventory_page_id'));
	}

	public static function getAddListingName(){
		$settings = get_option('stm_multilisting_settings', []);
		$add_listing_name = !empty($settings['add_listing_name']) ? $settings['add_listing_name'] : 'select-listing';
		return apply_filters('multilisting_add_listing_name', $add_listing_name);
	}

	// Filter categories of select post-type
	public static function STMListingsAttributesFilter($listing = '') {
		if(empty($listing))
			$listing = (new STMMultiListing())->getCurrentListing();
		if(!$listing) return;
		$options = "stm_{$listing['slug']}_options";

		$stm_attributes = function ($result, $args = array()) use ($options)
		{
			$args = wp_parse_args($args, array(
				'where' => array(),
				'key_by' => ''
			));

			$result = array();
			$data = array_filter((array)get_option($options));

			foreach ($data as $key => $_data) {
				$passed = true;
				foreach ($args['where'] as $_field => $_val) {
					if (array_key_exists($_field, $_data) && $_data[$_field] != $_val) {
						$passed = false;
						break;
					}
				}

				if ($passed) {
					if ($args['key_by']) {
						$result[$_data[$args['key_by']]] = $_data;
					} else {
						$result[] = $_data;
					}
				}
			}
			return $result;
		};
		add_filter('stm_listings_attributes', $stm_attributes, 1000, 2);
	}

	public function STMListingsFilter( $compact, $terms ) {
		if(!empty($compact['listing_title'])){
			$pt = $this->getCurrentListing();
			if($pt){
				$compact['listing_title'] = __("{$pt['name']} for sale", MULTILISTING_DOMAIN);
			}
		}
		return $compact;
	}

	public function queryArgsPostType($args, $source) {
		if($this->listings){
			foreach ($this->listings as $listing) {
				if($listing['slug'] == get_post_type()){
					$args['post_type'] = get_post_type();
				}
				if(!empty($source['posttype']) && $listing['slug'] == $source['posttype']){
					$args['post_type'] = $listing['slug'];
				}
			}
		}
		return $args;
	}

	public function addPostTypeSidebar() {
		$post_type = null;
		$listings = STMMultiListing::getListings();
		foreach ($listings as $listing) {
			if($listing['slug'] == get_post_type(get_the_ID())) $post_type = $listing['slug'];
		}
		if(!$post_type && !empty(get_query_var('listings_type'))){
			$post_type = get_query_var('listings_type');
		}
		if(!empty($post_type))
			echo '<input type="hidden" name="posttype" value="'.$post_type.'">';
	}

	public function addPostTypesUserProfileNavigation($nav) {
		if(!empty($this->listings)){
			$temp = [];
			$first_listing = reset($this->listings);
			foreach ($nav as $i => $n){
				if($i == 'favourite'){
					foreach ($this->listings as $key => $listing) {
						$temp[$listing['slug']] = array(
							'label' => __("My", MULTILISTING_DOMAIN) .'&nbsp;'. esc_html__( $listing['name'], MULTILISTING_DOMAIN ),
							'url' => add_query_arg( array( 'page' => $listing['slug'] ), stm_get_author_link( '' ) ),
							'icon' => 'stm-service-icon-inventory',
						);
					}
					$temp[$i] = $n;
				}
				$temp[$i] = $n;
			}

		}
		if(
			$this->settings && isset($this->settings['disable_orig_listing']) &&
			$this->settings['disable_orig_listing'] == "true" &&
			!empty($temp['inventory'])
		){
			global $wp;
			if(
				$temp['inventory']['url'] == home_url( $wp->request ).'/' &&
				empty($_REQUEST['page']) &&
				!isset($_REQUEST['view-myself']) &&
				!empty($first_listing)
			){
				?>
				<script type="application/javascript">
					document.location.href = '<?php echo add_query_arg(
					['page' => $first_listing['slug']],
					$temp['inventory']['url']
				); ?>'
				</script>
				<?php
			}
			unset($temp['inventory']);
		}
		return $temp;
	}

	public function replaceVehicleListingOption($value, $option) {
		$post_type = $this->getCurrentListing();
		if(!$post_type || $post_type == 'listings') return $value;
		if(!empty($this->listings)){
			foreach ($this->listings as $listing) {
				if($post_type['slug'] == $listing['slug'])
					return get_option("stm_{$listing['slug']}_options", []);
			}
		}
		return $value;
	}

	public function addPostTypeSelectRule(){
		add_rewrite_rule(
			"^{$this->add_listing_name}[/]?$",
			'index.php?add_custom_listing=index',
			'top'
		);
	}

	public function addPostTypeSelectWhiteList( $query_vars ){
		$query_vars[] = 'add_custom_listing';
		return $query_vars;
	}

	function addPostTypeSelect( $template ) {
		if ( get_query_var( 'add_custom_listing' ) == false || get_query_var( 'add_custom_listing' ) == '' ) {
			return $template;
		}
		return MULTILISTING_PATH . '/templates/select-add-listing.php';
	}

	function loadFrontEnqueueScriptsStyles(){
		wp_enqueue_style(
			'listing-frontend',
			MULTILISTING_PLUGIN_URL . '/assets/css/multilisting-frontend.css',
			NULL,
			'1.0.2',
			'all'
		);

	}

	protected function registerListings($listing) {
		register_post_type($listing['slug'], array(
			'labels' => array(
				'name' => __($listing['name'], MULTILISTING_DOMAIN),
				'singular_name' => __($listing['name'], MULTILISTING_DOMAIN),
				'add_new' => __('Add New', MULTILISTING_DOMAIN),
				'add_new_item' => __('Add New Item', MULTILISTING_DOMAIN),
				'edit_item' => __('Edit Item', MULTILISTING_DOMAIN),
				'new_item' => __('New Item', MULTILISTING_DOMAIN),
				'all_items' => __('All Items', MULTILISTING_DOMAIN),
				'view_item' => __('View Item', MULTILISTING_DOMAIN),
				'search_items' => __('Search Items', MULTILISTING_DOMAIN),
				'not_found' => __('No items found', MULTILISTING_DOMAIN),
				'not_found_in_trash' => __('No items found in Trash', MULTILISTING_DOMAIN),
				'parent_item_colon' => '',
				'menu_name' => __($listing['name'], MULTILISTING_DOMAIN),
			),
			'menu_icon' => 'dashicons-location-alt',
			'show_in_nav_menus' => true,
			'supports' => array('title', 'editor', 'thumbnail', 'comments', 'excerpt', 'author', 'revisions'),
			'rewrite' => array('slug' => $listing['slug']),
			'has_archive' => true,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'query_var' => true,
			'hierarchical' => false,
			'menu_position' => null,
		));
	}

	public function deregisterOriginalPostType() {
		if($this->settings && isset($this->settings['disable_orig_listing']) && $this->settings['disable_orig_listing'] == "true")
			unregister_post_type( 'listings' );
	}

	public function extendedListingPostType($listings) {
		if(is_admin()) return $listings;
		if(!isset($_REQUEST['page']) || $_REQUEST['page'] !== 'favourite') return $listings;
		$listings = explode(',', $listings);
		if(isset($this->settings['disable_orig_listing']) && $this->settings['disable_orig_listing'] == 'true'){
			unset($listings[array_search('listings', $listings)]);
		}
		if(!empty($this->listings)) {
			foreach ($this->listings as $listing) {
				$listings[] = $listing['slug'];
			}
		}

		return $listings;
	}

	public function addPostTypeOnInsertPost($post_data) {
		if(!empty($_REQUEST['post_type'])){
			$pre_moderation = $this->getListingSetting('pre_moderation', esc_attr($_REQUEST['post_type']));
			if($pre_moderation == 'true') $post_data['post_status'] = 'pending';
			$post_data['post_type'] = esc_attr($_REQUEST['post_type']);
		}
		return $post_data;
	}

	public function replaceNotifyOnAddListing($response) {
		$post_type = esc_attr($_REQUEST['post_type']);
		if(empty($post_type)) return $response;

		if($response['message'] == 'Car Added, uploading photos'){
			$response['message'] = sprintf(
				__('%s Added, uploading photos', MULTILISTING_DOMAIN),
				$this->getListingNameBySlug($post_type)
			);
		}
		if($response['message'] == 'Car Updated, uploading photos'){
			$response['message'] = sprintf(
				__('%s Updated, uploading photos', MULTILISTING_DOMAIN),
				$this->getListingNameBySlug($post_type)
			);
		}
		if(!empty($post_type))
			$response['post_type'] = $post_type;

		return $response;
	}

	public function replaceNotifyOnAddListingMedia($response) {
		if(!empty($_REQUEST['post_id'])){
			$post_type = get_post_type($_REQUEST['post_id']);
		}
		if($post_type == 'listings') return $response;

		if($response['message'] == 'Car updated, redirecting to your account profile'){
			$response['message'] = sprintf(
				__('%s updated, redirecting to your account profile', MULTILISTING_DOMAIN),
				$this->getListingNameBySlug($post_type)
			);
		}

		if($response['message'] == 'Car added, redirecting to your account profile'){
			$response['message'] = sprintf(
				__('%s added, redirecting to your account profile', MULTILISTING_DOMAIN),
				$this->getListingNameBySlug($post_type)
			);
		}

		if($response['message'] == 'Car added, redirecting to checkout'){
			$response['message'] = sprintf(
				__('%s added, redirecting to checkout', MULTILISTING_DOMAIN),
				$this->getListingNameBySlug($post_type)
			);
		}

		$redirectType = (isset($_POST['redirect_type'])) ? $_POST['redirect_type'] : '';
		if(empty($redirectType) || $redirectType !== 'pay'){
			$user_id = get_current_user_id();
			$response['url'] = esc_url(add_query_arg(
				['page' => $post_type],
				get_author_posts_url($user_id)
			));
		}
		return $response;
	}

	public function similarListingQuery( $query ) {
		$post_type = get_post_type(get_the_id());
		if($post_type !== 'listings'){
			$query['post_type'] = $post_type;
		}
		return $query;
	}

	public function replaceListingOnFavoritePage($query) {
		$list = ['listings'];
		if(!empty($this->listings)){
			foreach ($this->listings as $listing) {
				$list[] = $listing['slug'];
			}
		}
		if($this->settings && isset($this->settings['disable_orig_listing']) && $this->settings['disable_orig_listing'] == "true"){
			$key = array_search('listings', $list);
			unset($list[$key]);
		}
		if(!empty($_REQUEST['page']) && $_REQUEST['page'] == 'favourite'){
			$query->set( 'post_type', $list );
		}
	}

	function changeTranslateTextMultiple( $translated ) {
		$text = array(
			'Select Your Car Features' => 'Select Your Listing Features',
			'Car Details' => 'Listing Details',
			'Car Location' => 'Listing Location'
		);
		$translated = str_ireplace(  array_keys($text),  $text,  $translated );
		return $translated;
	}

	public function stm_filter_listing_link( $listing_link, $filters ){
		if(!empty($filters['ml_post_type'])){
			$listing_link = get_post_type_archive_link($filters['ml_post_type']);
			unset($filters['ml_post_type']);
			if(!empty($listing_link)){
				$qs = array();
				foreach ( $filters as $key => $val ) {
					$info = stm_get_all_by_slug( preg_replace( '/^(min_|max_)/',  '', $key ) );
					$val = (is_array($val)) ? implode(',', $val) : $val;
					$qs[] = $key . ( ! empty( $info['listing_rows_numbers'] ) ? '[]=' : '=' ) . $val;
				}

				if ( count( $qs ) ) {
					$listing_link .= (strpos($listing_link, '?') ? '&' : '?') . join( '&', $qs );
				}
			}
		}
		return $listing_link;
	}

	public function stm_binding_results_return_200( $r ) {
		header("HTTP/1.1 200 OK");
		return $r;
	}

	public function stm_register_perpay() {
		remove_filter( 'woocommerce_data_stores', 'woocommerce_data_stores' );
		add_filter( 'woocommerce_data_stores', array($this, 'woocommerce_data_stores') );
	}
	public function woocommerce_data_stores ( $stores ) {
		require_once MULTILISTING_PATH . "/classes/perpay.class.php";
		$stores['product'] = 'STM_Multi_Listing_Data_Store_CPT';
		return $stores;
	}

	public function enable_pay_per_listing($value) {
		$listings_type = get_query_var('listings_type');
		if(!empty($listings_type)){
			foreach ($this->listings as $listing) {
				if($listing['slug'] == $listings_type){
					if(!empty($listing['settings']['ppl']) && $listing['settings']['ppl'] == 'true'){
						return true;
					}
				}
			}
		}
		return $value;
	}

	public function disable_restriction_post_ppl($restrictions){
		$listings_type = get_query_var('listings_type');
		if(!empty($listings_type)) {
			foreach ($this->listings as $listing) {
				if ($listing['slug'] == $listings_type) {
					if (!empty($listing['settings']['ppl'])
						&& $listing['settings']['ppl'] == 'true') {
						$restrictions['posts'] = 0;
						return $restrictions;
					}
				}
			}
		}
		return $restrictions;
	}

	public function pay_per_listing_price($value) {
		$listings_type = get_query_var('listings_type');
		if(!empty($listings_type)){
			foreach ($this->listings as $listing) {
				if($listing['slug'] == $listings_type){
					if(!empty($listing['settings']['ppl_price'])){
						return $listing['settings']['ppl_price'];
					}
				}
			}
		}
		return $value;
	}

	public function stm_after_listing_gallery_saved($post_id) {
		set_query_var('listings_type', get_post_type($post_id));
	}

	public function stm_listings_inventory_page_id($page_id) {
		global $wp_query;
		if(!empty($wp_query->get('listings_type'))){
			$listing_id = $this->getListingSetting('inventory_id', $wp_query->get('listings_type'));
			if($listing_id)	return $listing_id;
		}
		return $page_id;
	}
}

new HooksMultiListing;
