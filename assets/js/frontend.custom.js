if (typeof (STMListings) == 'undefined') {
	var STMListings = {};
}
(function($) {
	'use strict';
	STMListings.Filter.prototype.performAjax = function (url) {
		let type = $('form[data-trigger="filter"]').find('input[name="posttype"]').val();
		$.ajax({
			url: url,
			dataType: 'json',
			context: this,
			data: 'ajax_action=' + this.ajax_action + '&posttype='+type,
			beforeSend: this.ajaxBefore,
			success: this.ajaxSuccess,
			complete: this.ajaxComplete
		});
	};
	//STMListings.Filter.prototype.ajax_action = STMListings.Filter.prototype.ajax_action +  + '&posttype='+type;
}(jQuery));
