let listing_list = new Vue({
	el: '#listing_list_app',
	data: {
		listings: [],
		settings: {
			disable_orig_listing: false,
			add_listing_name: ''
		},
		successSaved: false,
		pages: [],
		modalShow: false,
		modalIndex: 0,
		modal: {
			ppl: false,
			ppl_price: '10.00',
			ppl_limit: '14',
			featured_price: '3.00',
			featured_limit: 30,
			inventory_id: '',
			pre_moderation: false,
			generate_title: ''
		}
	},
	mounted () {
		if(stm_listings && stm_listings.listings){
			if(stm_listings.listings.length)
				this.listings = stm_listings.listings;
			else
				this.listings.push({
					name: '',
					slug: '',
					icon: '',
					page: '',
					settings: {}
				});
		}else{
			this.listings.push({
				name: '',
				slug: '',
				icon: '',
				page: '',
				settings: {}
			});
		}
		if(stm_listings && stm_listings.settings){
			this.settings = stm_listings.settings;
		}
		if(stm_listings && stm_listings.pages) {
			this.pages = stm_listings.pages;
		}
	},
	methods: {
		duplicate (index, event) {
			if(this.validate(index)){
				this.listings.push({
					name: '',
					slug: '',
					icon: '',
					page: 0,
					settings: {}
				});
			}
		},
		showSettingModal (index, event) {
			this.modalIndex = index;
			if(this.listings[index].settings){
				let settings = this.listings[index].settings;
				for(var i in this.modal){
					if(typeof settings[i] === 'undefined'){
						settings[i] = this.modal[i];
					}
				}
				// this.modal = Object.assign(this.modal, this.listings[index].settings);
				this.modal = settings;
				// this.modal.pre_moderation = this.modal.pre_moderation === 'true';
				// this.modal.ppl = this.modal.ppl === 'true';
			}
			console.log(this.modal);
			jQuery(this.$refs.modal).find('.modal-title').html('Settings of '+this.listings[index].name);
			this.modalShow = true;

		},
		applyListingSettings(event) {
			this.listings[this.modalIndex].settings = this.modal;
			this.modalShow = false;
		},
		validate (index) {
			let self = this;
			let element = self.$refs['el_'+index][0];
			this.listings.forEach(function(el, i){
				self.$refs['el_'+i+'_name'][0].classList.remove('error');
				self.$refs['el_'+i+'_slug'][0].classList.remove('error');
			});
			if(this.listings[index]){
				if(this.listings[index]['name'] !== ''){
					if(this.listings[index]['slug'] !== ''){
						return true;
					}else{
						self.$refs['el_'+index+'_slug'][0].classList.add('error');
					}
				}else{
					self.$refs['el_'+index+'_name'][0].classList.add('error');
				}
			}
			return false;
		},
		setSlug(index, event) {
			if(event.target.value){
				this.listings[index].slug = event.target.value
					.toString()                     // Cast to string
					.toLowerCase()                  // Convert the string to lowercase letters
					.normalize('NFD')       // The normalize() method returns the Unicode Normalization Form of a given string.
					.trim()                         // Remove whitespace from both sides of a string
					.replace(/\s+/g, '-')           // Replace spaces with -
					.replace(/[^\w\-]+/g, '')       // Remove all non-word chars
					.replace(/\-\-+/g, '-');
			}
		},
		remove (index, event) {
			if (event) {
				event.preventDefault();
				event.stopPropagation();
			}
			if(confirm('Are you sure you want to delete the type "'+this.listings[index]['name']+'"?'))
				this.listings.splice(index, 1);
		},
		submit (event) {
			let self = this;
			self.successSaved = false;
			jQuery.ajax({
			    type: 'POST',
			    url: ajaxurl,
			    data: {
			    	action: 'save_multilisting_data',
					listings: self.listings,
					settings: self.settings
				},
				context: this,
			    dataType: 'json',
			    success: function(data) {
			    	jQuery(self.$refs.application).removeClass('preloader');
			    	self.successSaved = true;
			        setTimeout(function(){
						self.successSaved = false;
					},2000);

			    },
			    error:function (xhr, ajaxOptions, thrownError){
					jQuery(self.$refs.application).removeClass('preloader');
			        console.log('error...', xhr);
			        //error logging
			    },
			    beforeSend: function(){
					jQuery(self.$refs.application).addClass('preloader');
			    }
			});
		}
	},
});


(function($) {
	'use strict'
	if(stm_listings && stm_listings.listings) {
		stm_listings.listings.forEach(function(list, index){
			// Save New Row
			$(document).on('click',
				'.stm_vehicles_add_new .listing_categories_add_new .stm_vehicles_listing_option_meta ' +
				'.stm_vehicles_listing_row_actions a[href="#add_new_'+list.slug+'"]', function () {
				var $stmForm = $(this).closest('form');
				var $stmtable = $(this).closest('.stm_vehicles_add_new');

				$.ajax({
					url: ajaxurl,
					type: 'POST',
					dataType: 'json',
					data: $stmForm.serialize() + '&action=stm_'+list.slug+'_add_new_option',
					context: this,
					beforeSend: function () {
						$('.listing_categories_add_new .stm_response_message').text();
						$('.listing_categories_add_new .stm_response_message').slideUp();
						$stmtable.addClass('loading');
					},
					success: function (data) {
						$stmtable.removeClass('loading');
						if (data.message) {
							$('.listing_categories_add_new .stm_response_message').text(data.message);
							$('.listing_categories_add_new .stm_response_message').slideDown();
						}
						if (data.option) {
							stm_post_type_append_option(list, data.option);
						}
					}
				});
			});

			// Delete Row
			$(document).on('click', '.stm_vehicles_listing_row_actions a[href="#delete_'+list.slug+'"]', function(e) {

				var numberTr = $(this).closest('tr').attr('data-tr');
				var $stmOptions = $(this).closest('.stm_vehicles_listing_option_meta');

				var confirm_delete = confirm('Are you sure?');
				if(!confirm_delete){
					e.preventDefault();
					return;
				}

				$.ajax({
					url: ajaxurl,
					type: 'POST',
					dataType: 'json',
					data: 'number=' + numberTr + '&action=stm_'+list.slug+'_delete_option_row',
					context: this,
					beforeSend: function () {
						$stmOptions.addClass('loading');
					},
					success: function (data) {
						$stmOptions.removeClass('loading');
						stmVehiclesListingDeleteOption(numberTr);
					}
				});
			});

			// Save Row
			$(document).on('click', '.stm_vehicles_listing_row_actions a[href="#save_'+list.slug+'"]', function() {
				var $stmForm = $(this).closest('form');
				var $stmOptions = $(this).closest('.stm_vehicles_listing_option_meta');

				$.ajax({
					url: ajaxurl,
					type: 'POST',
					dataType: 'json',
					data: $stmForm.serialize() + '&action=stm_'+list.slug+'_save_option_row',
					context: this,
					beforeSend: function () {
						$stmOptions.addClass('loading');
					},
					success: function (data) {
						$stmOptions.removeClass('loading');
					}
				});
			});

			var stmSortableSelectorM = $(".stm_vehicles_listing_categories .stm_vehicles_listing_content table."+list.slug+"_categories_edit tbody");

			$(stmSortableSelectorM).sortable({
				items: ".stm_listings_settings_head"
			});
			$(stmSortableSelectorM).disableSelection();

			$(stmSortableSelectorM).sortable({
				stop: function (event, ui) {

					var newOrder = $(this).sortable("toArray", {attribute: 'data-tr'});

					$('.'+list.slug+'_categories_edit .stm_listings_settings_tr').each(function(){
						var currentNum = $(this).attr('data-tr');

						$(this).detach().insertAfter($('.stm_listings_settings_head[data-tr="' + currentNum + '"]'));

					});

					$.ajax({
						url: ajaxurl,
						type: 'POST',
						dataType: 'json',
						data: 'order=' + newOrder.join(',') + '&action=stm_'+list.slug+'_save_option_order',
						context: this,
						beforeSend: function () {
							$('.stm_vehicles_listing_categories .stm_vehicles_listing_content').addClass('loading');
						},
						success: function (data) {
							$('.stm_vehicles_listing_categories .stm_vehicles_listing_content').removeClass('loading');
							stmVehiclesListingSortOptions(list);
						},
						error: function (jqXHR, exception) {
							var msg = '';
							if (jqXHR.status === 0) {
								msg = 'Not connect.\n Verify Network.';
							} else if (jqXHR.status === 404) {
								msg = 'Requested page not found. [404]';
							} else if (jqXHR.status === 500) {
								msg = 'Internal Server Error [500].';
							} else if (exception === 'parsererror') {
								msg = 'Requested JSON parse failed.';
							} else if (exception === 'timeout') {
								msg = 'Time out error.';
							} else if (exception === 'abort') {
								msg = 'Ajax request aborted.';
							} else {
								msg = 'Uncaught Error.\n' + jqXHR.responseText;
							}
							console.warn(msg);
						}
					});
				}
			}).disableSelection();


		})

	}


	function stmVehiclesListingSortOptions(list) {
		var number = 0;
		$('.stm_listings_settings_head').each(function(){
			$(this).attr('data-tr', number);
			number ++;
		});

		number = 0;

		$('.'+list.slug+'_categories_edit .stm_listings_settings_tr').each(function(){
			$(this).attr('data-tr', number);
			$(this).find('input[name="stm_vehicle_listing_row_position"]').val(number);
			number ++;
		});
	}

	function stmVehiclesListingDeleteOption(number) {

		$('.stm_listings_settings_head[data-tr="' + number + '"], ' +
			'.stm_listings_settings_tr[data-tr="' + number + '"]').remove();
	}

	function stm_post_type_append_option(list, option) {
		var trHead = '<tr class="stm_listings_settings_head" data-tr="' + option.key + '">' +
			'<td class="highlited">' + option.name + '</td> ' +
			'<td>' + option.plural + '</td> ' +
			'<td>' + option.slug + '</td> ' +
			'<td>' + option.numeric + '</td> ' +
			'<td class="manage"><i class="fa fa-list-ul" data-url="' + option.link + '"></i></td> ' +
			'<td><i class="fa fa-pencil"></i></td> ' +
			'</tr>';

		var tableHead = '.'+list.slug+'_categories_edit tbody';
		var $tableHead = $(tableHead);

		$tableHead.append(trHead);
		var $newTrSettings = $('.listing_categories_add_new tr').clone();
		$newTrSettings.appendTo(tableHead);
		$newTrSettings.attr('data-tr', option.key);
		$newTrSettings.find('.stm_vehicles_listing_option_meta').hide();

		$('.listing_categories_add_new form').trigger('reset');

		$newTrSettings.find('.stm_response_message').text();
		$newTrSettings.find('.stm_response_message').hide();

	}
}(jQuery));

